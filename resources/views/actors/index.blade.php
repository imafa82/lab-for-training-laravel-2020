@if(!empty($actors))
    <ul>
        @foreach($actors as $actor)
            <li style="{{$loop->first ? 'background: red' : ''}}">
                {{$loop->iteration}}) {{$actor->name}}
            </li>
        @endforeach
    </ul>
@endif
