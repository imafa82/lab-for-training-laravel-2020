<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // users - GET
        // restituisce l'elenco degli utenti

        return 'Lista Utenti';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // users/create - GET
        // restituisce il form di creazione dell'utente
        //<form action="/users" method="POST"
        return 'form nuovo utente';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // users - POST
        // creare un nuovo utente
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // users/3 - GET
        // restituisce dati utente con id 3

        return 'Utente con id 3';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // users/3/edit - GET
        // restituisce il form di modifica dell'utente
        // <form action="users/3" method="POST"
        // @method('PUT')
        return 'Form modifica utente '.$id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // users/3 - PUT o PATCH
        // modificherà l'utente

        return 'Modifica singolo utente';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // users/3 - DELETE
        // Eliminerà a DB l'utente 3
    }
}
