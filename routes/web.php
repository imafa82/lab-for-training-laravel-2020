<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('table/{n?}', 'MultipleTableController@index')
    ->where('n', '[0-9]+')->name('table');

Route::get('/hello/{name?}/{surname?}/{year?}', function ($name = 'Massy', $surname = 'Salerno', $year = 1982){
    $name = ucfirst($name);
    $surname = ucfirst($surname);
    return view('hello', compact('name', 'surname', 'year'));
})
    ->where('name', '[a-z]+')
    ->where('surname', '[a-z,A-Z]+')
    ->where('year', '[0-9]+');

//Route::get('users', 'UserController@index');
//Route::post('users', 'UserController@store');
//Route::get('users/create', 'UserController@create');
//Route::get('users/{id}/edit', 'UserController@edit');
//Route::put('users/{id}', 'UserController@update');
//Route::delete('users/{id}', 'UserController@destroy');
//

Route::resource('users', 'UserController');
Route::resource('actors', 'ActorController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
