<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->save('Massimiliano', 'imafa82@gmail.com');
        $this->save('Fabrizio', 'fabrizio@gmail.com');
    }

    public function save($name, $email, $password = 'test'){
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();

    }
}
